import React from 'react';
import "./categoryCard.scss";
const CategoryCard = ({name,img}) => {
	return (
		<div className="category_card">
			<img src={img} alt="IMG" className="img-fluid"/>
			<p className="category_name">{name}</p>
		</div>
	);
};

export default CategoryCard;