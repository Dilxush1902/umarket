import React from 'react';
import "./style.css";
import {MdOutlineShoppingCart, MdSyncAlt} from "react-icons/md"
import {FiHeart} from "react-icons/fi"
import {AiFillStar} from "react-icons/ai"

const ProductCard = ({img}) => {
	return (
		<div className="card top_card">
			<img src={img} className="card-img-top img-fluid" alt="..." />
			<div className="card-body">
				<h5 className="card-title">Samsung Galaxy A41 Red <br/>
					64 GB</h5>
				<p className="card-text ">3 144 000 сум</p>
				<p className="card-text ">От 385 000 сум/12 мес</p>
				<div className="stars">
					<span className="star"><AiFillStar/></span>
					<span className="star"><AiFillStar/></span>
					<span className="star"><AiFillStar/></span>
					<span className="star"><AiFillStar/></span>
					<span className="star"><AiFillStar/></span>
				</div>
				<div className="d-flex justify-content-between">
					<button className="card_btn btn btn-primary"><MdOutlineShoppingCart/> <span>В корзину</span></button>
					<span>
						<button className="btn" ><MdSyncAlt/></button>
						<button className="btn" ><FiHeart/></button>
					</span>
				</div>
			</div>
		</div>
	);
};

export default ProductCard;