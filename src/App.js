import React from "react";
import "./App.css";
import Header from "./Layout/Header";
import NavBar from "./Layout/NavBar";
import TopBar from "./Layout/TopBar";
import Banner from "./Layout/Banner";
import CategoryTop from "./Layout/Categories/CategoryTop";
import ProductTop from "./Layout/Products/Product_Top";
import CategoryBottom from "./Layout/Categories/CategoryBottom";
import ProductBottom from "./Layout/Products/Product_Bottom";
import Brands from "./Layout/Brands";
import AppMarket from "./Layout/AppMarket";
import Footer from "./Layout/Footer";

const App = () => {
	return (
		<main>
			<TopBar/>
			<Header/>
			<NavBar/>
			<div className="content">
				<Banner/>
				<CategoryTop name={"Популярные категории"}/>
				<ProductTop name={"Хиты продаж"}/>
				<CategoryBottom name={"Техника для дома"}/>
				<ProductBottom name={"Выбор покупателей"}/>
				<Brands/>
				<AppMarket/>
			</div>
			<Footer/>
		</main>);
};

export default App;
