import React from 'react';
import "./style.css";

const NavBar = () => {

	return (
		<div className="main_nav_bar ">
			<div className="nav_bar nav-scroller container py-1 mb-2">
				<nav className="nav d-flex flex-wrap justify-content-center justify-content-md-start  ">
					<a className=" pe-3 me-md-4  link-secondary" href="#">Акции и скидки</a>
					<a className=" pe-3 me-md-4  link-secondary" href="#">Смартфоны и гаджеты</a>
					<a className=" pe-3 me-md-4  link-secondary" href="#">Телевизоры и аудио</a>
					<a className=" pe-3 me-md-4  link-secondary" href="#">Техника для кухни</a>
					<a className=" pe-3 me-md-4  link-secondary" href="#">Красота и здоровье</a>
					<a className=" pe-3 me-md-4 link-secondary" href="#">Ноутбуки и компьютеры</a>
				</nav>
			</div>
		</div>
	);
};

export default NavBar;