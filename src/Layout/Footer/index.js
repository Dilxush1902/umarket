import React from 'react';
import "./footer.css";
import Humo from "../../Assets/payment_svg/humo.png"
import UzCard from "../../Assets/payment_svg/uz-card.png";
import PayMe from "../../Assets/payment_svg/payme.png";
import {FiLinkedin, FiInstagram, FiTwitter, FiFacebook} from "react-icons/fi"

const Footer = () => {
	return (
		<footer className="max_width">
			<div className="wrapper_footer">
				<div className="row_top row">
					<div className="col-2">
						<div className="col_name">Компания</div>
						<ul>
							<li><a className='text-decoration-none' href="#">О компании</a></li>
							<li><a className='text-decoration-none' href="#">Адреса магазинов</a></li>
						</ul>
					</div>
					<div className="col-2">
						<div className="col_name">Информация</div>
						<ul>
							<li><a className='text-decoration-none' href="#">Рассрочка</a></li>
							<li><a className='text-decoration-none' href="#">Доставка </a></li>
							<li><a className='text-decoration-none' href="#">Бонусы</a></li>
						</ul>
					</div>
					<div className="col">
						<div className="col_name">Помощь покупателю</div>
						<ul>
							<li><a className='text-decoration-none' href="#">Вопросы и ответы</a></li>
							<li><a className='text-decoration-none' href="#">Как сделать заказ на сайте</a></li>
							<li><a className='text-decoration-none' href="#">Обмен и возврат</a></li>
						</ul>
					</div>
					<div className="col-3">
						<div className="col_name">Способ оплаты</div>
						<ul className="nav">
							<li className="nav-link ps-0 pe-1"><a className='text-decoration-none' href="#"><img src={PayMe} alt=""/></a>
							</li>
							<li className="nav-link ps-2 pe-1"><a className='text-decoration-none' href="#"><img src={Humo} alt=""/></a></li>
							<li className="nav-link ps-2 pe-1"><a className='text-decoration-none' href="#"><img src={UzCard} alt=""/></a>
							</li>
						</ul>
					</div>
					<div className="col">
						<div className="col_name">Мы в социальных сетях</div>
						<ul className="nav">
							<li className="nav-link ps-0 pe-2"><a href="#"><FiLinkedin/></a></li>
							<li className="nav-link px-3	"><a href="#"><FiInstagram/></a></li>
							<li className="nav-link px-3"><a href="#"><FiTwitter/></a></li>
							<li className="nav-link lpx-3"><a href="#"><FiFacebook/></a></li>
						</ul>
					</div>
				</div>
				<div className="row_center row">
					<div className="col-10">
						<div className="row">
							<div className="col-3">
								<div className="col_name">Компания</div>
								<ul>
									<li><a href="#">+9980 71-54-60-60</a></li>
								</ul>
							</div>
							<div className="col-9">
								<div className="col_name">Почта для пожеланий и предложений</div>
								<ul>
									<li><a href="#">info@udevsmarket.com</a></li>
								</ul>
							</div>
						</div>
					</div>

				</div>
				<div className="row_bottom row">
					<div className="col-12 text-center">
						<p><span>UdevsMarket.uz</span> Все права защищены</p>
					</div>
				</div>
			</div>
		</footer>
	);
};

export default Footer;