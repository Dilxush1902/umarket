import React from 'react';
import "./categoryBottom.scss";
import CategoryCard from "../../../Components/Cards/CardCategory";
import img1 from "./category_bottom_img/1.png"
import img2 from "./category_bottom_img/2.png"
import img3 from "./category_bottom_img/3.png"
import img4 from "./category_bottom_img/4.png"
import img5 from "./category_bottom_img/5.png"

const CategoryBottom = ({name}) => {
	return (
		<section className="max_width">
			<div className="wrapper_category ">
				<h2 className="section_name">{name}</h2>
				<div className="category_bottom">
					<div className="category_item">
						<CategoryCard name={"Встраиваемая техника"} img={img1}/>
					</div>
					<div className="category_item">
						<CategoryCard name={"Встраиваемая техника"} img={img2}/>
					</div>
					<div className="category_item">
						<CategoryCard name={"Встраиваемая техника"} img={img3}/>
					</div>
					<div className="category_item">
						<CategoryCard name={"Встраиваемая техника"} img={img4}/>
					</div>
					<div className="category_item">
						<CategoryCard name={"Встраиваемая техника"} img={img5}/>
					</div>
				</div>
			</div>
		</section>
	);
};

export default CategoryBottom;