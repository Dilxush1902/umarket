import React from 'react';
import "./category.scss";
import img1 from "./category_top_img/1.png"
import img2 from "./category_top_img/2.png"
import img3 from "./category_top_img/3.png"
import img4 from "./category_top_img/4.png"
import CategoryCard from "../../../Components/Cards/CardCategory";


const CategoryTop = ({name}) => {
	return (
		<section className="max_width">
			<div className="wrapper_category ">
				<h2 className="section_name">{name}</h2>
				<div className="category">
					<CategoryCard img={img1} name={"Смартфоны"}/>
					<CategoryCard img={img2} name={"Мониторы"}/>
					<CategoryCard img={img3} name={"Компьютеры"}/>
					<CategoryCard img={img4} name={"Аксессуары"}/>
				</div>
			</div>
		</section>
	);
};

export default CategoryTop;