import React, {useState} from 'react';
import "./style.css";
import {MdOutlineLocationOn, MdOutlinePhone} from "react-icons/md";
import ReactFlagsSelect from "react-flags-select";

const TopBar = () => {
	const [selected, setSelected] = useState("RU");
	console.log(selected)
	return (
		<section className="wrapper_top_bar">
			<div className="top_bar py-1">
				<nav className="d-flex flex-wrap justify-content-between align-items-center">
					<ul className="nav mx-auto mx-md-0">
						<li className="nav-item"><a href="#" className="nav-link  px-2 pe-2" aria-current="page">Магазины</a></li>
						<li className="nav-item"><a href="#" className="nav-link  px-2">Оставить отзыв</a></li>
						<li className="nav-item"><a href="#" className="nav-link  px-2">Доставка</a></li>
					</ul>
					<ul className="nav fs-5 mx-auto mx-md-0 d-flex align-items-center">
						<li className="nav-item"><a href="#" className="nav-link px-2  pe-2"><span><MdOutlinePhone/></span> +998 97
							778-17-08</a></li>
						<li className="nav-item"><a href="#" className="nav-link  px-2"><span><MdOutlineLocationOn/></span>Ташкент</a>
						</li>
						<li className="nav-item">
							<ReactFlagsSelect
								className="py-0"
								countries={["RU", "UZ", "FR", "DE", "IT"]}
								customLabels={{RU: "RU", UZ: "UZ", FR: "FR", DE: "DE", IT: "IT"}}
								selected={selected}
								onSelect={(code) => setSelected(code)}
							/>
						</li>
					</ul>
				</nav>
			</div>
		</section>
	);
};

export default TopBar;