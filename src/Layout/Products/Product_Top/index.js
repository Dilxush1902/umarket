import React from 'react';
import "./product.scss";
import CardProduct from "../../../Components/Cards/CardProduct";
import img1 from "../img_card/phone1.png"
import img2 from "../img_card/phone2.png"

const ProductTop = ({name}) => {
	return (
		<section className="max_width">
				<div className="wrapper_product">
					<h2 className="section_name">{name}</h2>
						<div className="product">
								<CardProduct img={img1}/>
								<CardProduct img={img1}/>
								<CardProduct img={img1}/>
								<CardProduct img={img2}/>
						</div>
				</div>
		</section>
	);
};

export default ProductTop;