import React from 'react';
import "./appMarket.css";
import u_market from "../../Assets/mobile_app_svg/umarket.svg";
import qr_kode from "../../Assets/mobile_app_svg/qr-kode.svg";
import google_play from "../../Assets/mobile_app_svg/googlePlay.svg";
import app_store from "../../Assets/mobile_app_svg/appStore.svg"
import screen_p from "../../Assets/mobile_app_svg/screen.svg";

const AppMarket = () => {
	return (
		<section className="app_mobile_wrap">
			<div className="max_width">
				<div className="row app_mobile">
					<div className="col-5 left_wrap">
						<div className="row py-1">
							<div className="col">
								<div className="top_text">
									<img src={u_market} alt=""/>
									<span></span>
									<span>mobile application</span>
								</div>
							</div>
						</div>
						<div className="row py-1">
							<div className="center_text">
								Заказывайте через
								мобильное приложение
							</div>
						</div>
						<div className="row py-3">
							<div className="col ">
								<img src={google_play} className="pe-1" alt=""/>
								<img src={app_store} alt=""/>
							</div>
						</div>
					</div>
					<div className="col center_wrap">
						{/*<img src={screen_p} alt="Screen"/>*/}
						<div className="center_qr">
							<div className="py-2">
								<img src={u_market} className="img-fluid " alt="u-market"/>
							</div>
							<div className="py-2">
								<img src={qr_kode} className="img-fluid" alt="qr-kode"/>
							</div>
						</div>
					</div>
					<div className="col right_wrap">
						<p>Отсканируйте <span>QR</span>-код
							и установите приложение</p>
					</div>
				</div>
			</div>
		</section>
	);
};

export default AppMarket;