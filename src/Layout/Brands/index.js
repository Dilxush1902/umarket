import React from 'react';
import "./brand.scss";
import huawei from "./brand_img/huawei.png"
import nokia from "./brand_img/nokia.png";
import iphone from "./brand_img/iphone.png"
import mi from "./brand_img/mi.png"
import lg from "./brand_img/lg.png"
import samsung from "./brand_img/samsung.png"

const Brands = () => {
	return (
		<section className=" max_width">
			<div className="wrapper_brand">
				<h2 className="section_name">Популярные бренды</h2>
				<div className="brands">
					<div className="brand_name">
						<div className="brand_name_item">
							<p>Телефоны</p>
						</div>
						<div className="brand_name_item">
							<p>Аксессуары</p>
						</div>
						<div className="brand_name_item">
							<p>Premium</p>
						</div>
						<div className="brand_name_item">
							<p>Спорт</p>
						</div>
						<div className="brand_name_item">
							<p>Игрушки</p>
						</div>
						<div className="brand_name_item">
							<p>Красота</p>
						</div>
						<div className="brand_name_item">
							<p>Книги</p>
						</div>
						<div className="brand_name_item">
							<p>Обувь</p>
						</div>
					</div>
				</div>
				<div className="brands_logo">
					<div className="brand_logo_item">
						<img src={nokia} className="img-fluid" alt=""/>
					</div>
					<div className="brand_logo_item">
						<img src={iphone} className="img-fluid" alt=""/>
					</div>
					<div className="brand_logo_item">
						<img src={samsung} className="img-fluid" alt=""/>
					</div>
					<div className="brand_logo_item">
						<img src={samsung} className="img-fluid" alt=""/>
					</div>
					<div className="brand_logo_item">
						<img src={samsung} className="img-fluid" alt=""/>
					</div>
					<div className="brand_logo_item">
						<img src={samsung} className="img-fluid" alt=""/>
					</div>
				</div>
				<div className="brands_logo">
					<div className="brand_logo_item">
						<img src={huawei} className="img-fluid" alt=""/>
					</div>
					<div className="brand_logo_item">
						<img src={lg} className="img-fluid" alt=""/>
					</div>
					<div className="brand_logo_item">
						<img src={mi} className="img-fluid" alt=""/>
					</div>
					<div className="brand_logo_item">
						<img src={mi} className="img-fluid" alt=""/>
					</div>
					<div className="brand_logo_item">
						<img src={mi} className="img-fluid" alt=""/>
					</div>
					<div className="brand_logo_item">
						<img src={mi} className="img-fluid" alt=""/>
					</div>
				</div>
			</div>
		</section>
	);
};

export default Brands;