import React from 'react';
import "./style.scss";
import {MdShoppingCart} from "react-icons/md"
import {FiHeart} from "react-icons/fi";
import {MdSyncAlt, MdLogin, MdOutlinePhotoSizeSelectActual} from "react-icons/md";
import {AiOutlineSearch} from "react-icons/ai"
import Logo from "./udevs.png";

const Header = () => {
	return (
		<header>
			<div className="header px-1 py-2 bg-white text-white">
				<div
					className="d-flex  align-items-center flex-column flex-lg-row justify-content-center justify-content-lg-between">
					<a href="/" className="d-flex align-items-center col-md-2 my-2 my-lg-0  text-white text-decoration-none">
						<div className="bi me-2" width="40" height="32" role="img">
							<img src={Logo} alt="Logo"/>
						</div>
					</a>
					<form className=" d-flex col-12  col-md-auto col-lg-6 mx-lg-3 my-2 justify-content-center align-items-center ">
						<span><AiOutlineSearch/></span>
						<input type="text" className=" form-control border-0 shadow-none" placeholder="Поиск по товарам"/>
						<span><MdOutlinePhotoSizeSelectActual/></span>
					</form>
					<ul className="nav  col-md-12 col-lg-auto my-2 justify-content-center my-md-0 text-small">
						<li>
							<a href="#" className="nav-link ">
								<svg className="bi d-block mx-auto mb-1 fs-5" width="24" height="24">
									<MdShoppingCart/>
								</svg>
								Корзина
							</a>
						</li>
						<li>
							<a href="#" className="nav-link">
								<svg className="bi d-block mx-auto mb-1 fs-5" width="24" height="24">
									<FiHeart/>
								</svg>
								Избранные
							</a>
						</li>
						<li>
							<a href="#" className="nav-link ">
								<svg className="bi d-block mx-auto mb-1 fs-5" width="24" height="24">
									<MdSyncAlt/>
								</svg>
								Сравнение
							</a>
						</li>
						<li>
							<a	 href="#" className="nav-link  ">
								<svg className="bi d-block mx-auto mb-1 fs-5" width="24" height="24">
									<MdLogin/>
								</svg>
								Войти
							</a>
						</li>
					</ul>
				</div>
			</div>
		</header>);
};

export default Header;