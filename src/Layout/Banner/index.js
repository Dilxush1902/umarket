import React from 'react';
import ImgCarousel1 from "./img_carousel/image 2.png"
import "./style.css";

const Banner = () => {
	return (
		<section className="banner">
			<div id="carouselExampleIndicators" className="carousel slide"
								data-bs-ride="carousel">
				<div className="carousel-indicators ">
					<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0"
													className="active rounded-circle"
													aria-current="true" aria-label="Slide 1"></button>
					<button className="rounded-circle" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
													aria-label="Slide 2"></button>
					<button className="rounded-circle" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
													aria-label="Slide 3"></button>
					<button className="rounded-circle" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3"
													aria-label="Slide 4"></button>
				</div>
				<div className="carousel-inner ">
					<div className="carousel-item active" data-bs-interval="1000">
						<img src={ImgCarousel1} className="d-block w-100 border_radius" alt="..."/>
					</div>
					<div className="carousel-item" data-bs-interval="2000">
						<img src={ImgCarousel1} className="d-block w-100 border_radius" alt="..."/>
					</div>
					<div className="carousel-item" data-bs-interval="3000">
						<img src={ImgCarousel1} className="d-block w-100 border_radius" alt="..."/>
					</div>
					<div className="carousel-item" data-bs-interval="4000">
						<img src={ImgCarousel1} className="d-block w-100 border_radius" alt="..."/>
					</div>
				</div>
			</div>
		</section>
	);
};

export default Banner;